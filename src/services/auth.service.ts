import fs from "fs";
import accounts from "../data/account.json";

export const signIn = (username: string, pasword: string) => {
  for (let account of accounts) {
    if (account.username === username) {
      if (account.password === pasword) {
        return { status: true, role: account.role };
      }
    }
  }

  return {
    status: false,
    message: "Tài khoản hoặc mật khẩu không đúng",
    code: "USERNAME_OR_PASSWORD_INCORRECT",
  };
};

export const signUp = (username: string, password: string) => {
  const isUsernameExist = accounts.some((acc) => acc.username === username);

  if (isUsernameExist) {
    return {
      status: false,
      message: "Tài khoản đã tồn tại",
      code: "USERNAME_IS_EXIST",
    };
  }

  accounts.push({
    username,
    password,
    role: 2,
  });

  fs.writeFileSync("../../data/acounts.json", JSON.stringify(accounts));
};
