import {
  AppBar,
  Toolbar,
  Typography,
  Button,
  Container,
  Dialog,
  DialogActions,
  DialogTitle,
  Badge,
  IconButton,
} from "@mui/material";
import { Box } from "@mui/system";
import React, { Component } from "react";
import { connect } from "react-redux";
import { Outlet } from "react-router";
import { Link } from "react-router-dom";
import { AnyAction, bindActionCreators, Dispatch } from "redux";
import { RootState } from "../../reducers";
import HomeIcon from "@mui/icons-material/Home";
import { getBackupProducts, IProduct, loginAuth } from "../../actions";
import ShoppingCartOutlinedIcon from "@mui/icons-material/ShoppingCartOutlined";

interface IState {
  isOpenDialogLogout: boolean;
}

interface IProps
  extends ReturnType<typeof mapDispatchToProps>,
    ReturnType<typeof mapStateToProps> {
  navigation: any;
}

export class Navirgator extends Component<IProps, IState> {
  constructor(props: any) {
    super(props);

    this.state = {
      isOpenDialogLogout: false,
    };
  }

  componentWillMount = () => {
    const username = localStorage.getItem("username");
    const role = Number(localStorage.getItem("role")) || 0;

    if (username && role) {
      this.props.loginAuth({ username, role });
    }

    const productsStr = localStorage.getItem("products") || "[]";
    const products: IProduct[] = JSON.parse(productsStr);
    this.props.getBackupProducts(products);
  };

  handleLogout = () => {
    localStorage.removeItem("username");
    localStorage.removeItem("role");

    this.props.loginAuth({ username: "", role: 0 });
    this.setState({ isOpenDialogLogout: false });
    this.props.navigation("/");
  };

  handleToCart = () => {
    this.props.navigation("cart");
  };

  render() {
    const { username, role } = this.props.authReducer;
    const productLength =
      this.props.cartReducer[username]?.products.length || 0;
    return (
      <>
        <Box sx={{ flexGrow: 1 }}>
          <AppBar position="static">
            <Container>
              <Toolbar>
                <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                  <Link
                    to="/"
                    style={{ textDecoration: "none", color: "#fff" }}
                  >
                    <HomeIcon />
                  </Link>
                </Typography>
                {role === 2 ? (
                  <IconButton onClick={this.handleToCart}>
                    <Badge badgeContent={productLength} color="error">
                      <ShoppingCartOutlinedIcon />
                    </Badge>
                  </IconButton>
                ) : (
                  <></>
                )}
                {username !== "" ? (
                  <>
                    <p style={{ paddingRight: "20px", paddingLeft: "50px" }}>
                      Hello, {username}
                    </p>
                    <p>
                      <Button
                        variant="outlined"
                        color="error"
                        onClick={() =>
                          this.setState({ isOpenDialogLogout: true })
                        }
                      >
                        Log out
                      </Button>
                    </p>
                  </>
                ) : (
                  <Button variant="outlined" color="inherit">
                    <Link
                      to="/login"
                      style={{ textDecoration: "none", color: "#fff" }}
                    >
                      Login
                    </Link>
                  </Button>
                )}
              </Toolbar>
            </Container>
          </AppBar>

          <Outlet />
        </Box>

        <Dialog
          open={this.state.isOpenDialogLogout}
          onClose={() => this.setState({ isOpenDialogLogout: false })}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">
            {`Do you sure to log out ?`}
          </DialogTitle>
          <DialogActions>
            <Button
              variant="outlined"
              color="error"
              onClick={this.handleLogout}
            >
              Log out
            </Button>
            <Button
              onClick={() => this.setState({ isOpenDialogLogout: false })}
              autoFocus
            >
              Cancel
            </Button>
          </DialogActions>
        </Dialog>
      </>
    );
  }
}

const mapStateToProps = (state: RootState) => {
  return {
    authReducer: state.authReducer,
    cartReducer: state.cartReducer,
  };
};

const mapDispatchToProps = (dispatch: Dispatch<AnyAction>) =>
  bindActionCreators({ loginAuth, getBackupProducts }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Navirgator);
