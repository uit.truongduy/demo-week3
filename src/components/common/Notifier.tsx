import { Snackbar, Alert, AlertColor } from "@mui/material";
import React, { Component } from "react";

interface IProp {
  open: boolean;
  handleClose: any;
  message: string;
  alertColor: AlertColor;
}

export class Notifier extends Component<IProp, {}> {
  render() {
    return (
      <Snackbar
        open={this.props.open}
        autoHideDuration={6000}
        onClose={this.props.handleClose}
        anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
      >
        <Alert
          onClose={() => this.props.handleClose()}
          severity={this.props.alertColor}
          sx={{ width: "100%" }}
        >
          {this.props.message}
        </Alert>
      </Snackbar>
    );
  }
}

export default Notifier;
