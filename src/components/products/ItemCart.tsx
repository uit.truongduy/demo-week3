import DeleteIcon from "@mui/icons-material/Delete";
import {
  Button,
  Dialog,
  DialogActions,
  DialogTitle,
  TableCell,
  TableRow,
} from "@mui/material";
import React, { Component } from "react";
import { connect } from "react-redux";
import { AnyAction, bindActionCreators, Dispatch } from "redux";
import { changeQuantityCart } from "../../actions";
import { RootState } from "../../reducers";

interface IState {
  isOpenDialogDelete: boolean;
}

interface IProps
  extends ReturnType<typeof mapDispatchToProps>,
    ReturnType<typeof mapStateToProps> {
  product: {
    productId: string;
    productName: string;
    productPrice: number;
    productQuantity: number;
  };
}

export class ItemCart extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);

    this.state = {
      isOpenDialogDelete: false,
    };
  }

  handlerChangeQuantity = (value: number) => {
    const { username } = this.props.authReducer;
    this.props.changeQuantityCart({
      username,
      product: {
        product_id: this.props.product.productId,
        quantity: value,
      },
    });
  };

  render() {
    const { productName, productPrice, productQuantity } = this.props.product;
    return (
      <TableRow>
        <TableCell align="left">{productName}</TableCell>

        <TableCell align="left">{productPrice} &nbsp;($)</TableCell>
        <TableCell align="center">
          <Button
            variant="outlined"
            color="secondary"
            size="small"
            onClick={() => this.handlerChangeQuantity(-1)}
            disabled={productQuantity === 1}
          >
            -
          </Button>
          {productQuantity}
          <Button
            variant="outlined"
            color="secondary"
            size="small"
            onClick={() => this.handlerChangeQuantity(1)}
          >
            +
          </Button>
        </TableCell>
        <TableCell align="left">
          {productPrice * productQuantity} &nbsp;($)
        </TableCell>
        <TableCell align="left">
          <Button
            variant="outlined"
            color="error"
            style={{ marginLeft: "5px" }}
            startIcon={<DeleteIcon />}
            onClick={() => this.setState({ isOpenDialogDelete: true })}
          >
            Delete
          </Button>
        </TableCell>

        <Dialog
          open={this.state.isOpenDialogDelete}
          onClose={() => this.setState({ isOpenDialogDelete: false })}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">
            {`Do you sure to delete ${productName} ?`}
          </DialogTitle>
          <DialogActions>
            <Button
              variant="outlined"
              color="error"
              onClick={() =>
                this.handlerChangeQuantity(-this.props.product.productQuantity)
              }
            >
              Delete
            </Button>
            <Button
              onClick={() => this.setState({ isOpenDialogDelete: false })}
              autoFocus
            >
              Cancel
            </Button>
          </DialogActions>
        </Dialog>
      </TableRow>
    );
  }
}

const mapStateToProps = (state: RootState) => {
  return {
    authReducer: state.authReducer,
    cartReducer: state.cartReducer,
  };
};

const mapDispatchToProps = (dispatch: Dispatch<AnyAction>) =>
  bindActionCreators({ changeQuantityCart }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ItemCart);
