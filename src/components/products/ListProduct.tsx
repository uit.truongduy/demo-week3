import {
  TableContainer,
  Paper,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
} from "@mui/material";
import React, { Component } from "react";
import { connect } from "react-redux";
import { AnyAction, bindActionCreators, Dispatch } from "redux";
import { getBackupProducts, getListProducts, IProduct } from "../../actions";
import { RootState } from "../../reducers";
import ProductRow from "./ProductRow";

interface IState {}

interface IProps
  extends ReturnType<typeof mapDispatchToProps>,
    ReturnType<typeof mapStateToProps> {}

export class ListProduct extends Component<IProps, IState> {
  componentWillMount = () => {
    const productsStr = localStorage.getItem("products") || "[]";
    const products: IProduct[] = JSON.parse(productsStr);
    this.props.getBackupProducts(products);
  };

  componentWillUnmount = () => {
    localStorage.setItem(
      "products",
      JSON.stringify(this.props.productReducer?.products)
    );
  };

  render() {
    const products: IProduct[] = this.props.productReducer?.products;
    return (
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Product name</TableCell>
              <TableCell align="left">Category</TableCell>
              <TableCell align="left">price</TableCell>
              <TableCell align="left">Image</TableCell>
              <TableCell align="left">Tags</TableCell>
              <TableCell align="left">Actions</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {products.map((product) => (
              <ProductRow product={product} key={product.id} />
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    );
  }
}

const mapStateToProps = (state: RootState) => {
  return {
    productReducer: state.productReducer,
  };
};

const mapDispatchToProps = (dispatch: Dispatch<AnyAction>) =>
  bindActionCreators(
    {
      getListProducts,
      getBackupProducts,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(ListProduct);
