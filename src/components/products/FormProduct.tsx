import {
  Autocomplete,
  Avatar,
  Box,
  Button,
  Checkbox,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  MenuItem,
  TextField,
} from "@mui/material";
import React, { Component } from "react";
import { connect } from "react-redux";
import { AnyAction, bindActionCreators, Dispatch } from "redux";
import { createProduct, editProduct, IProduct } from "../../actions";
import { RootState } from "../../reducers";
import randomstring from "randomstring";
import CheckBoxOutlineBlankIcon from "@mui/icons-material/CheckBoxOutlineBlank";
import CheckBoxIcon from "@mui/icons-material/CheckBox";

const icon = <CheckBoxOutlineBlankIcon fontSize="small" />;
const checkedIcon = <CheckBoxIcon fontSize="small" />;

interface IState {
  isOpenForm: boolean;
  productId: string;
  productCategory: string;
  productTitle: string;
  productDescription: string;
  image_url: string;
  productPrice: number;
  productTags: string[];
}

interface IProps
  extends ReturnType<typeof mapDispatchToProps>,
    ReturnType<typeof mapStateToProps> {
  openForm: boolean;
  handleClose: Function;
  action?: string;
  productInfo?: IProduct;
}

export class FormProduct extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);

    this.state = {
      isOpenForm: props.openForm,
      productCategory: props.productInfo?.category || "",
      image_url: props.productInfo?.image_url || "",
      productId: props.productInfo?.id || "",
      productTitle: props.productInfo?.title || "",
      productDescription: props.productInfo?.description || "",
      productPrice: props.productInfo?.price || 0,
      productTags: props.productInfo?.tags || [],
    };
  }

  handleCloseForm = () => {
    this.props.handleClose();
    this.resetForm();
  };

  handleChangeCategory = (e: any) => {
    this.setState({
      productCategory: e.target.value,
    });
  };

  randomImage = (e: any) => {
    this.setState({
      image_url: "https://picsum.photos/200/300",
    });
  };

  hanlderChangeTag = (e: any, value: string[]) => {
    this.setState({
      productTags: value,
    });
  };

  handleSubmit = (e: any) => {
    e.preventDefault();
    const {
      productId,
      image_url,
      productTitle,
      productCategory,
      productDescription,
      productPrice,
      productTags,
    } = this.state;

    if (this.props.action === "create") {
      this.props.createProduct({
        id: productId !== "" ? productId : randomstring.generate(),
        category: productCategory,
        description: productDescription,
        image_url,
        price: productPrice,
        tags: productTags,
        title: productTitle,
        rating: 0,
      });
      this.resetForm();
    } else {
      this.props.editProduct({
        id: productId !== "" ? productId : randomstring.generate(),
        category: productCategory,
        description: productDescription,
        image_url,
        price: productPrice,
        tags: productTags,
        title: productTitle,
        rating: 0,
      });
    }

    this.props.handleClose();
  };

  resetForm = () => {
    this.setState({
      productCategory: "",
      image_url: "",
      productId: "",
      productTitle: "",
      productDescription: "",
      productPrice: 0,
      productTags: [],
    });
  };

  render() {
    const categories = this.props.productReducer?.categories || [];
    const tags = this.props.productReducer?.tags || [];
    const title =
      this.props.action === "create"
        ? "Create new product"
        : "Edit product information";
    return (
      <Dialog
        open={this.props.openForm}
        onClose={this.handleCloseForm}
        maxWidth={"md"}
      >
        <DialogTitle>{title}</DialogTitle>
        <DialogContent>
          <Box
            component="form"
            noValidate
            onSubmit={this.handleSubmit}
            sx={{ mt: 3 }}
            style={{}}
          >
            <Grid container spacing={2}>
              <Grid item xs={12} sm={8}>
                <TextField
                  required
                  fullWidth
                  id="title"
                  label="Product Name"
                  name="title"
                  autoComplete="title"
                  value={this.state.productTitle}
                  onChange={(e) =>
                    this.setState({ productTitle: e.target.value })
                  }
                />
              </Grid>
              <Grid item xs={12} sm={4}>
                <TextField
                  select
                  fullWidth
                  label="Category"
                  value={this.state.productCategory}
                  onChange={this.handleChangeCategory}
                  helperText="Please select category"
                >
                  {categories.map((category: string, index: number) => (
                    <MenuItem key={index} value={category}>
                      {category}
                    </MenuItem>
                  ))}
                </TextField>
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  multiline
                  rows={4}
                  id="description"
                  label="Decription"
                  name="description"
                  autoComplete="description"
                  value={this.state.productDescription}
                  onChange={(e) =>
                    this.setState({ productDescription: e.target.value })
                  }
                />
              </Grid>
              <Grid item xs={12} sm={4}>
                <TextField
                  required
                  fullWidth
                  name="price"
                  label="Price"
                  type="number"
                  id="price"
                  autoComplete="price"
                  value={this.state.productPrice}
                  onChange={(e) =>
                    this.setState({ productPrice: +e.target.value })
                  }
                />
              </Grid>
              <Grid item xs={0} sm={2} />
              <Grid item xs={6} sm={2}>
                {this.state.image_url === "" ? (
                  <Avatar
                    variant="rounded"
                    sx={{ width: 56, height: 56 }}
                    style={{ width: 100, height: 100 }}
                  />
                ) : (
                  <Avatar
                    variant="rounded"
                    sx={{ width: 56, height: 56 }}
                    style={{ width: 100, height: 100 }}
                    src={this.state.image_url}
                  />
                )}
              </Grid>
              <Grid item xs={6} sm={3}>
                <Button
                  variant="contained"
                  component="span"
                  onClick={this.randomImage}
                >
                  Random image
                </Button>
              </Grid>

              <Grid item xs={12} sm={4}>
                <Autocomplete
                  multiple
                  id="tags"
                  options={tags}
                  disableCloseOnSelect
                  getOptionLabel={(option) => option}
                  renderOption={(props, option, { selected }) => (
                    <li {...props}>
                      <Checkbox
                        icon={icon}
                        checkedIcon={checkedIcon}
                        style={{ marginRight: 8 }}
                        checked={selected}
                      />
                      {option}
                    </li>
                  )}
                  onChange={(event, value) =>
                    this.hanlderChangeTag(event, value)
                  }
                  style={{ width: 500 }}
                  value={this.state.productTags}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      label="Tags"
                      placeholder="Choose tag"
                    />
                  )}
                />
              </Grid>
            </Grid>
            <DialogActions>
              <Button
                type="submit"
                variant="contained"
                sx={{ mt: 3, mb: 2 }}
                onClick={(e) => this.handleSubmit(e)}
              >
                Submit
              </Button>
              <Button
                variant="contained"
                sx={{ mt: 3, mb: 2 }}
                onClick={this.handleCloseForm}
              >
                Cancel
              </Button>
            </DialogActions>
          </Box>
        </DialogContent>
      </Dialog>
    );
  }
}

const mapStateToProps = (state: RootState) => {
  return {
    productReducer: state.productReducer,
  };
};

const mapDispatchToProps = (dispatch: Dispatch<AnyAction>) =>
  bindActionCreators(
    {
      createProduct,
      editProduct,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(FormProduct);
