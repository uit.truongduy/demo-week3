import {
  AlertColor,
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  CardMedia,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  Typography,
} from "@mui/material";
import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { AnyAction, bindActionCreators, Dispatch } from "redux";
import { IProduct } from "../../actions";
import { RootState } from "../../reducers";
import { IAuthState } from "../../reducers/auth.reducer";
import { ICartState } from "../../reducers/cart.reducer";
import Notifier from "../common/Notifier";

interface IState {
  openNotify: boolean;
  notifyMessage: string;
  notifyType: AlertColor;
  openDialogDetail: boolean;
}

interface IProps
  extends ReturnType<typeof mapDispatchToProps>,
    ReturnType<typeof mapStateToProps> {
  product: IProduct;
  authReducer: IAuthState;
  cartReducer: ICartState;
  addToCart: any;
}

export class Item extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);

    this.state = {
      openNotify: false,
      notifyMessage: "",
      notifyType: "info",
      openDialogDetail: false,
    };
  }

  handleCloseNotify = (event: any, reason: string) => {
    if (reason === "clickaway") {
      return;
    }

    this.setState({
      openNotify: false,
    });
  };

  handleAddToCart = () => {
    const { username } = this.props.authReducer;
    if (username === "") {
      this.setState({
        openNotify: true,
        notifyMessage: "Login to add product to cart",
        notifyType: "error",
      });

      return;
    }

    this.props.addToCart({
      username,
      product: {
        product_id: this.props.product.id,
        quantity: 1,
      },
    });

    this.setState({
      openNotify: true,
      notifyMessage: "Add to cart success",
      notifyType: "success",
    });
  };

  handlerDetailProduct = () => {};

  render() {
    const { image_url, title, description, price, id, category } =
      this.props.product;
    return (
      <Grid item xs={12} md={4}>
        <Card sx={{ maxWidth: 345 }} onClick={this.handlerDetailProduct}>
          <CardMedia
            component="img"
            height="170"
            image={image_url}
            alt={title}
          />
          <CardContent>
            <Typography gutterBottom variant="h5" component="div">
              <p style={{ fontWeight: "bold", fontSize: "12" }}>{title}</p>
            </Typography>
            <Typography variant="body2" color="text.secondary">
              {description}
            </Typography>
          </CardContent>
          <CardActions
            style={{
              display: "flex",
              justifyContent: "space-between",
              margin: "5px",
            }}
          >
            <p>{price}$</p>
            <p>
              <Button
                size="small"
                variant="outlined"
                color="primary"
                onClick={() => this.setState({ openDialogDetail: true })}
              >
                See detail
              </Button>
              <Button
                size="small"
                variant="outlined"
                color="error"
                onClick={this.handleAddToCart}
              >
                Add to cart
              </Button>
            </p>
          </CardActions>
        </Card>

        <Notifier
          open={this.state.openNotify}
          handleClose={this.handleCloseNotify}
          alertColor={this.state.notifyType}
          message={this.state.notifyMessage}
        />

        <Dialog
          fullWidth
          maxWidth="sm"
          open={this.state.openDialogDetail}
          onClose={() => this.setState({ openDialogDetail: false })}
        >
          <DialogTitle>
            <div>
              <span>{title} -</span>{" "}
              <span style={{ color: "red" }}>{price} $</span>
            </div>
          </DialogTitle>
          <DialogContent>
            <Box
              noValidate
              component="form"
              sx={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "space-between",
              }}
            >
              <Grid xs={12} md={4}>
                <span>
                  <img
                    style={{ width: 100, height: 100 }}
                    src={image_url}
                    alt={title}
                  />
                </span>
              </Grid>
              <Grid xs={12} md={8}>
                <div>category: {category}</div>
                <span>description: {description}</span>
              </Grid>
            </Box>
          </DialogContent>
          <DialogActions>
            <Button size="small" variant="outlined" color="primary">
              <Link
                to={`post/${category}/${id}`}
                style={{ textDecoration: "none" }}
              >
                See review
              </Link>
            </Button>
            <Button
              size="small"
              variant="outlined"
              color="primary"
              onClick={() => this.setState({ openDialogDetail: true })}
            >
              <Link
                to={`/post/review/${id}`}
                style={{ textDecoration: "none" }}
              >
                Post review
              </Link>
            </Button>
            <Button
              size="small"
              variant="outlined"
              color="error"
              onClick={this.handleAddToCart}
            >
              Add to cart
            </Button>
            <Button onClick={() => this.setState({ openDialogDetail: false })}>
              Close
            </Button>
          </DialogActions>
        </Dialog>
      </Grid>
    );
  }
}

const mapStateToProps = (state: RootState) => {
  return {};
};

const mapDispatchToProps = (dispatch: Dispatch<AnyAction>) =>
  bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Item);
