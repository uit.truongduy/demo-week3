import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import {
  Avatar,
  Button,
  CircularProgress,
  Dialog,
  DialogActions,
  DialogTitle,
  TableCell,
  TableRow,
} from "@mui/material";
import React, { Component, Suspense } from "react";
import { connect } from "react-redux";
import { AnyAction, bindActionCreators, Dispatch } from "redux";
import { deleteProduct, editProduct } from "../../actions";
import { RootState } from "../../reducers";

const FormProduct = React.lazy(() => import("../products/FormProduct"));

interface IState {
  isOpenDialogDelete: boolean;
  isOpenDialogEdit: boolean;
}

interface IProps
  extends ReturnType<typeof mapDispatchToProps>,
    ReturnType<typeof mapStateToProps> {
  product: {
    id: string;
    title: string;
    image_url: string;
    price: number;
    description: string;
    category: string;
    tags: string[];
  };
}

export class ProductRow extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);

    this.state = {
      isOpenDialogDelete: false,
      isOpenDialogEdit: false,
    };
  }

  handleDeleteProduct = () => {
    this.props.deleteProduct(this.props.product.id);
  };

  handleCloseEditForm = () => {
    this.setState({
      isOpenDialogEdit: false,
    });
  };

  render() {
    const { id, title, image_url, price, category, tags } = this.props.product;
    return (
      <>
        <TableRow key={id}>
          <TableCell align="left">{title}</TableCell>

          <TableCell align="left">{category}</TableCell>
          <TableCell align="left">{price} &nbsp;($)</TableCell>

          <TableCell align="left">
            <Avatar
              variant="rounded"
              sx={{ width: 56, height: 56 }}
              style={{ width: 50, height: 50 }}
              src={image_url}
            />
          </TableCell>
          <TableCell align="left">
            {tags.map((tag) => {
              return <p>{tag}</p>;
            })}
          </TableCell>
          <TableCell align="left">
            <Button
              variant="outlined"
              color="secondary"
              startIcon={<EditIcon />}
              onClick={() => this.setState({ isOpenDialogEdit: true })}
            >
              Edit
            </Button>
            <Button
              variant="outlined"
              color="error"
              style={{ marginLeft: "5px" }}
              onClick={() => this.setState({ isOpenDialogDelete: true })}
              startIcon={<DeleteIcon />}
            >
              Delete
            </Button>
          </TableCell>
        </TableRow>
        <Dialog
          open={this.state.isOpenDialogDelete}
          onClose={() => this.setState({ isOpenDialogDelete: false })}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">
            {`Do you sure to delete ${title} ?`}
          </DialogTitle>
          <DialogActions>
            <Button
              variant="outlined"
              color="error"
              onClick={this.handleDeleteProduct}
            >
              Delete
            </Button>
            <Button
              onClick={() => this.setState({ isOpenDialogDelete: false })}
              autoFocus
            >
              Cancel
            </Button>
          </DialogActions>
        </Dialog>

        <Suspense fallback={<CircularProgress />}>
          <FormProduct
            openForm={this.state.isOpenDialogEdit}
            handleClose={this.handleCloseEditForm}
            action={"edit"}
            productInfo={this.props.product}
          />
        </Suspense>
      </>
    );
  }
}

const mapStateToProps = (state: RootState) => {
  return {
    productReducer: state.productReducer,
  };
};

const mapDispatchToProps = (dispatch: Dispatch<AnyAction>) =>
  bindActionCreators(
    {
      editProduct,
      deleteProduct,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(ProductRow);
