import { AnyAction } from "redux";
import { LOGIN_AUTH } from "../actions";

export interface IAuthState {
  username: string;
  role: number;
}

const initialProductState: IAuthState = {
  username: "",
  role: 0,
};

export default function productReducer(
  state = initialProductState,
  action: AnyAction
) {
  const { payload } = action;
  switch (action.type) {
    case LOGIN_AUTH: {
      return (state = { ...state, ...payload });
    }
    default:
      return { ...state };
  }
}
