import { AnyAction } from "redux";
import { ADD_TO_CART, CHANGE_QUANTITY_CART } from "../actions";

export interface ICartState {
  [key: string]: {
    products: {
      product_id: string;
      quantity: number;
    }[];
    productsLength: number;
  };
}

const initialProductState: ICartState = {};

export default function productReducer(
  state = initialProductState,
  action: AnyAction
) {
  const { payload } = action;
  const newState: ICartState = JSON.parse(JSON.stringify(state));
  switch (action.type) {
    case ADD_TO_CART: {
      const { username } = payload;
      const { product_id, quantity } = payload.product;
      // if user already has shopping cart
      const userCart = newState[username];
      if (userCart) {
        const indexProductCart = userCart.products.findIndex(
          (p) => p.product_id === product_id
        );

        if (indexProductCart !== -1) {
          userCart.products[indexProductCart].quantity += quantity;
        } else {
          userCart.products.push({
            product_id,
            quantity,
          });
          ++userCart.productsLength;
        }
      } else {
        newState[username] = {
          products: [
            {
              product_id,
              quantity,
            },
          ],
          productsLength: quantity,
        };
      }

      return (state = { ...newState });
    }
    case CHANGE_QUANTITY_CART: {
      const { username } = payload;
      const { product_id, quantity } = payload.product;
      console.log("payload", payload);
      const userProductsCart = newState[username].products;

      const indexProductCart = userProductsCart.findIndex(
        (p) => p.product_id === product_id
      );

      if (userProductsCart[indexProductCart].quantity + quantity <= 0) {
        userProductsCart.splice(indexProductCart, 1);
      } else {
        userProductsCart[indexProductCart].quantity += quantity;
      }
      console.log("newState", newState);

      return (state = { ...newState });
    }
    default:
      return { ...state };
  }
}
