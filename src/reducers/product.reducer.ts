import { AnyAction } from "redux";
import {
  GET_LIST_PRODUCT,
  CREATE_PRODUCT,
  EDIT_PRODUCT,
  DELETE_PRODUCT,
  IProduct,
  GET_BACKUP_PRODUCTS,
} from "../actions";

interface IProductState {
  products: IProduct[];
  categories: string[];
  tags: string[];
}

const initialProductState: IProductState = {
  products: [],
  categories: ["phone", "laptop", "tablet"],
  tags: ["iphone", "xiaomi", "ios", "android", "64GB", "128GB"],
};

export default function productReducer(
  state = initialProductState,
  action: AnyAction
) {
  const { payload } = action;
  switch (action.type) {
    case GET_BACKUP_PRODUCTS: {
      if (payload.length !== 0) state = { ...state, products: [...payload] };
      return state;
    }
    case GET_LIST_PRODUCT: {
      return { ...state };
    }
    case CREATE_PRODUCT: {
      const newProducts = JSON.parse(JSON.stringify(state.products));
      newProducts.push(payload);
      return (state = { ...state, products: newProducts });
    }
    case EDIT_PRODUCT: {
      const newProducts: IProduct[] = JSON.parse(
        JSON.stringify(state.products)
      );

      const index = newProducts.findIndex((p) => p.id === payload.id);
      newProducts[index] = { ...payload };

      return (state = { ...state, products: newProducts });
    }
    case DELETE_PRODUCT: {
      const newProducts = state.products.filter((p) => p.id !== payload);
      return (state = { ...state, products: newProducts });
    }
    default:
      return { ...state };
  }
}
