import { configureStore } from "@reduxjs/toolkit";
import productReducer from "./product.reducer";
import authReducer from "./auth.reducer";
import cartReducer from "./cart.reducer";
import reviewReducer from "./review.reducer";

export const store = configureStore({
  reducer: {
    productReducer,
    authReducer,
    cartReducer,
    reviewReducer,
  },
});

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch;
