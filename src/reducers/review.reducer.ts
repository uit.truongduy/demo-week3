import { AnyAction } from "@reduxjs/toolkit";
import { CREATE_REVIEW } from "../actions";

interface IReviewState {
  [key: string]: {
    [key: string]: {
      rating: number;
      review: string;
      timestamp: Date;
    };
  };
}

const initialProductState: IReviewState = {};

export default function reviewReducer(
  state = initialProductState,
  action: AnyAction
) {
  const newState = JSON.parse(JSON.stringify(state));
  switch (action.type) {
    case CREATE_REVIEW: {
      const { productId, username, rating, review } = action.payload;
      const productReview = newState[productId];
      if (productReview) {
        newState[productId][username] = {
          rating,
          review,
          timestamp: Date.now(),
        };
      } else {
        newState[productId] = {};
        newState[productId][username] = {
          rating,
          review,
          timestamp: Date.now(),
        };
      }
      console.log("newState", newState);
      return (state = { ...newState });
    }
    default:
      return { ...state };
  }
}
