import React from "react";
import { Routes, Route } from "react-router-dom";
import { useNavigate } from "react-router";

import Navirgator from "./components/common/Navirgator";
import Admin from "./pages/Admin";
import Home from "./pages/Home";
import Login from "./pages/Login";
import NoMatch from "./pages/NoMatch";
import Cart from "./pages/Cart";
import { PostReview } from "./pages/PostReview";
import ListReview from "./pages/ListReview";

function App() {
  const navigation = useNavigate();
  return (
    <Routes>
      <Route path="/" element={<Navirgator navigation={navigation} />}>
        <Route index element={<Home />} />
        <Route path="login" element={<Login navigation={navigation} />} />
        <Route path="admin" element={<Admin navigation={navigation} />} />
        <Route path="cart" element={<Cart />} />
        <Route path="post/review/:id" element={<PostReview />} />
        <Route path="post/:catecory/:id" element={<ListReview />} />

        {/* Using path="*"" means "match anything", so this route
                acts like a catch-all for URLs that we don't have explicit
                routes for. */}
        <Route path="*" element={<NoMatch />} />
      </Route>
    </Routes>
  );
}

export default App;
