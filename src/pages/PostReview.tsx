import React, { useEffect, useState } from "react";

import { Link, useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../reducers";
import {
  Alert,
  Button,
  Container,
  Grid,
  Rating,
  Snackbar,
  TextField,
  Typography,
} from "@mui/material";
import { createReview, IProduct } from "../actions";
import { Box } from "@mui/system";
import { useNavigate } from "react-router-dom";

export const PostReview: React.FC = (props: any) => {
  const params = useParams();
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const productReducer = useSelector(
    (state: RootState) => state.productReducer
  );
  const authReducer = useSelector((state: RootState) => state.authReducer);

  const [product, setProduct] = useState<IProduct>();
  const [rating, setRating] = useState(0);
  const [review, setReview] = useState("");
  const [openNotify, setOpenNotify] = useState(false);

  useEffect(() => {
    const productIndex = productReducer.products.findIndex(
      (p: IProduct) => p.id === params.id
    );

    if (productIndex !== -1) setProduct(productReducer.products[productIndex]);
  }, []);

  const handleSubmit = (e: any) => {
    e.preventDefault();

    dispatch(
      createReview({
        productId: product?.id || "",
        username: authReducer.username,
        rating,
        review,
      })
    );

    setOpenNotify(true);

    setTimeout(() => {
      navigate(`/post/${product?.category}/${product?.id}`);
    }, 1000);
  };

  return (
    <Container>
      {product ? (
        <>
          <Grid container spacing={2} sx={{ mt: 3 }} style={{ margin: "5px" }}>
            <Grid xs={12} md={4}>
              <img
                style={{ width: 250, height: 250 }}
                src={product?.image_url}
                alt={product?.title}
              />
            </Grid>
            <Grid xs={12} md={8}>
              <p style={{ fontWeight: "bold", fontSize: "20px" }}>
                {product?.title}
              </p>
              <p>{product?.description}</p>
              <br />
              <p style={{ color: "red" }}>{product?.price} $</p>
            </Grid>
          </Grid>

          <Box
            component="form"
            noValidate
            onSubmit={handleSubmit}
            sx={{ mt: 3 }}
            style={{}}
          >
            <Grid container spacing={2}>
              <Grid item xs={12} sm={4}>
                <Typography component="legend">Rating</Typography>
                <Rating
                  name="simple-controlled"
                  value={rating}
                  onChange={(event, newValue) => {
                    setRating(newValue || rating);
                  }}
                />
              </Grid>

              <Grid item xs={12} sm={12}>
                <TextField
                  required
                  fullWidth
                  multiline
                  id="review"
                  label="Type review"
                  name="review"
                  rows={4}
                  value={review}
                  onChange={(e) => setReview(e.target.value)}
                />
              </Grid>
            </Grid>
            <Grid>
              <Button
                type="submit"
                variant="contained"
                sx={{ mt: 3, mb: 2, mr: 3 }}
                color="error"
              >
                Submit
              </Button>
              <Button variant="contained" sx={{ mt: 3, mb: 2 }}>
                <Link to="/" style={{ textDecoration: "none", color: "#fff" }}>
                  Back
                </Link>
              </Button>
            </Grid>
          </Box>
        </>
      ) : (
        <div>Product not exist</div>
      )}

      <Snackbar
        open={openNotify}
        autoHideDuration={6000}
        onClose={() => setOpenNotify(false)}
        anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
      >
        <Alert
          onClose={() => setOpenNotify(false)}
          severity="success"
          sx={{ width: "100%" }}
        >
          Post review success
        </Alert>
      </Snackbar>
    </Container>
  );
};
