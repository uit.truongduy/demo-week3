import {
  Container,
  CssBaseline,
  Avatar,
  Typography,
  TextField,
  Button,
} from "@mui/material";
import { Box } from "@mui/system";
import React, { Component } from "react";
import { connect } from "react-redux";
import { AnyAction, bindActionCreators, Dispatch } from "redux";
import { RootState } from "../reducers";
import { signIn } from "../services/auth.service";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import { loginAuth } from "../actions";

interface IProps
  extends ReturnType<typeof mapDispatchToProps>,
    ReturnType<typeof mapStateToProps> {
  navigation: any;
}

export class Login extends Component<IProps, {}> {
  handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    const username = data.get("email")?.toString();
    const password = data.get("password")?.toString();

    if (username && password) {
      const { status, role = 0 } = signIn(username, password);
      console.log("{ status, role }", { status, role });
      if (status) {
        localStorage.setItem("username", username);
        localStorage.setItem("role", role + "");
        this.props.loginAuth({ username, role });

        role === 1
          ? this.props.navigation("/admin")
          : this.props.navigation("/");
      }
    }
  };

  componentWillMount = () => {
    const username = localStorage.getItem("username");
    const role = Number(localStorage.getItem("role")) || 0;

    if (username && role) {
      role === 1 ? this.props.navigation("/admin") : this.props.navigation("/");

      this.props.loginAuth({ username, role });
    }
  };

  render() {
    return (
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          <Box
            component="form"
            onSubmit={(e: any) => this.handleSubmit(e)}
            noValidate
            sx={{ mt: 1 }}
          >
            <TextField
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email Address"
              name="email"
              autoComplete="email"
              autoFocus
            />
            <TextField
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >
              Sign In
            </Button>
          </Box>
        </Box>
      </Container>
    );
  }
}

const mapStateToProps = (state: RootState) => {
  return {
    authReducer: state.authReducer,
  };
};

const mapDispatchToProps = (dispatch: Dispatch<AnyAction>) =>
  bindActionCreators({ loginAuth }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Login);
