import { Container, Grid, Rating } from "@mui/material";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { IProduct } from "../actions";
import { RootState } from "../reducers";

function ListReview() {
  const params = useParams();
  const productReducer = useSelector(
    (state: RootState) => state.productReducer
  );
  const authReducer = useSelector((state: RootState) => state.authReducer);
  const reviewReducer = useSelector((state: RootState) => state.reviewReducer);

  const [product, setProduct] = useState<IProduct>();
  const [reviews, setReviews] = useState<{
    [key: string]: {
      rating: number;
      review: string;
      timestamp: Date;
    };
  }>();

  useEffect(() => {
    const productIndex = productReducer.products.findIndex(
      (p: IProduct) => p.id === params.id
    );

    if (productIndex !== -1) {
      const productFound = productReducer.products[productIndex];
      setProduct(productFound);
      setReviews(reviewReducer[productFound.id]);
    }
  });

  const reviewsLength: number = Object.keys(reviews || {}).length;
  return (
    <Container>
      {product ? (
        <>
          <Grid container spacing={2} sx={{ mt: 3 }} style={{ margin: "5px" }}>
            <Grid xs={12} md={4}>
              <img
                style={{ width: 250, height: 250 }}
                src={product?.image_url}
                alt={product?.title}
              />
            </Grid>
            <Grid xs={12} md={8}>
              <p style={{ fontWeight: "bold", fontSize: "20px" }}>
                {product?.title}
              </p>
              <p>{product?.description}</p>
              <br />
              <p style={{ color: "red" }}>{product?.price} $</p>
            </Grid>
          </Grid>
        </>
      ) : (
        <div>Product not exist</div>
      )}

      {reviewsLength > 0 ? (
        Object.keys(reviews || {}).map((user) => (
          <Grid
            container
            spacing={2}
            style={{
              border: "1px solid #3BAFDA",
              borderRadius: "15px",
              marginTop: "20px",
              padding: "10px",
            }}
          >
            <Grid
              xs={12}
              md={6}
              style={{ fontWeight: "bold", fontSize: "20px" }}
            >
              {authReducer.username === user ? "You" : authReducer.username}
            </Grid>

            <Grid xs={12} md={6}>
              {reviews ? new Date(reviews[user].timestamp).toISOString() : ""}
            </Grid>
            <Grid xs={12} md={6}>
              {reviews ? reviews[user].review : ""}
            </Grid>
            <Grid xs={12} md={6}>
              <Rating
                name="simple-controlled"
                value={reviews ? reviews[user].rating : 3}
                disabled
              />
            </Grid>
          </Grid>
        ))
      ) : (
        <p>No have reviewer</p>
      )}
    </Container>
  );
}
export default ListReview;
