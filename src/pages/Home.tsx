import { Container, Grid } from "@mui/material";
import React, { Component } from "react";
import { connect } from "react-redux";
import { AnyAction, bindActionCreators, Dispatch } from "redux";
import { addToCart, IProduct } from "../actions";
import { Item } from "../components/products/Item";
import { RootState } from "../reducers";
import { getBackupProducts } from "../actions";

interface IState {}

interface IProps
  extends ReturnType<typeof mapDispatchToProps>,
    ReturnType<typeof mapStateToProps> {}

export class Home extends Component<IProps, IState> {
  componentWillMount = () => {
    const productsStr = localStorage.getItem("products") || "[]";
    const products: IProduct[] = JSON.parse(productsStr);
    this.props.getBackupProducts(products);
  };

  render() {
    const { products } = this.props.productReducer || [];
    return (
      <Container maxWidth="lg" sx={{ mt: 4, mb: 4 }}>
        <Grid container spacing={3}>
          {products.map((p: IProduct) => (
            <Item
              product={p}
              authReducer={this.props.authReducer}
              cartReducer={this.props.cartReducer}
              addToCart={this.props.addToCart}
            />
          ))}
        </Grid>
      </Container>
    );
  }
}

const mapStateToProps = (state: RootState) => {
  return {
    productReducer: state.productReducer,
    authReducer: state.authReducer,
    cartReducer: state.cartReducer,
  };
};

const mapDispatchToProps = (dispatch: Dispatch<AnyAction>) =>
  bindActionCreators(
    {
      getBackupProducts,
      addToCart,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(Home);
