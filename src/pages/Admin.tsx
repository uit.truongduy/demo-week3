import { Container, Button, CircularProgress } from "@mui/material";
import React, { Component, Suspense } from "react";
import { connect } from "react-redux";
import { AnyAction, bindActionCreators, Dispatch } from "redux";
import { loginAuth } from "../actions";
import ListProduct from "../components/products/ListProduct";
import { RootState } from "../reducers";

const FormProduct = React.lazy(
  () => import("../components/products/FormProduct")
);

interface IState {
  isOpenForm: boolean;
}

interface IProps
  extends ReturnType<typeof mapDispatchToProps>,
    ReturnType<typeof mapStateToProps> {
  navigation: any;
}

export class Admin extends Component<IProps, IState> {
  constructor(props: any) {
    super(props);

    this.state = {
      isOpenForm: false,
    };
  }

  handleCloseForm = () => {
    this.setState({
      isOpenForm: false,
    });
  };

  componentWillMount = () => {
    const role = Number(localStorage.getItem("role")) || 0;

    if (role !== 1) this.props.navigation("login");
  };

  render() {
    if (this.props.authReducer.role !== 1) this.props.navigation("login");
    return (
      <Container>
        <div
          style={{
            backgroundColor: "#3BAFDA", //#3BAFDA 4FC1E9
            display: "flex",
            flexDirection: "row",
            justifyContent: "center",
            marginTop: "10px",
          }}
        >
          <div
            style={{
              flexGrow: 8,
              paddingLeft: "20px",
              color: "#fff",
            }}
          >
            <h3>List Product</h3>
          </div>
          <Button
            variant="outlined"
            style={{
              marginRight: "20px",
              color: "#fff",
              border: "1px solid #fff",
              margin: "10px",
            }}
            onClick={() => this.setState({ isOpenForm: true })}
          >
            Create new product
          </Button>
        </div>

        <ListProduct />

        <Suspense fallback={<CircularProgress />}>
          <FormProduct
            openForm={this.state.isOpenForm}
            handleClose={this.handleCloseForm}
            action={"create"}
          />
        </Suspense>
      </Container>
    );
  }
}

const mapStateToProps = (state: RootState) => {
  return {
    authReducer: state.authReducer,
  };
};

const mapDispatchToProps = (dispatch: Dispatch<AnyAction>) =>
  bindActionCreators({ loginAuth }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Admin);
