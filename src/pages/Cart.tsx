import {
  Container,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@mui/material";
import React, { Component } from "react";
import { connect } from "react-redux";
import { AnyAction, bindActionCreators, Dispatch } from "redux";
import {
  changeQuantityCart,
  getBackupProducts,
  getListProducts,
  IProduct,
} from "../actions";
import ItemCart from "../components/products/ItemCart";
import { RootState } from "../reducers";

interface IProductsCart {
  productId: string;
  productName: string;
  productPrice: number;
  productQuantity: number;
}

interface IState {
  total: number;
  productsCart: IProductsCart[];
  message: string;
}

interface IProps
  extends ReturnType<typeof mapDispatchToProps>,
    ReturnType<typeof mapStateToProps> {}

export class Cart extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);

    this.state = {
      total: 0,
      productsCart: [],
      message: "Your cart have no products",
    };
  }

  componentWillMount = () => {
    const { username } = this.props.authReducer;
    this.loadCart(username);
  };

  loadCart = (username: string) => {
    if (!username || username === "")
      this.setState({
        message: "Login to get your cart",
      });

    const userCart = this.props.cartReducer[username];

    if (!userCart || userCart.productsLength === 0) return;

    const { products } = this.props.productReducer;
    const productsCart: IProductsCart[] = [];

    const total = userCart.products.reduce((pre, cur) => {
      const indexProduct = products.findIndex(
        (p: IProduct) => p.id === cur.product_id
      );

      const { title, price, id } = products[indexProduct];

      productsCart.push({
        productId: id,
        productName: title,
        productPrice: price,
        productQuantity: cur.quantity,
      });
      return pre + price * cur.quantity;
    }, 0);

    this.setState({ total, productsCart });
  };

  //WARNING! To be deprecated in React v17. Use new lifecycle static getDerivedStateFromProps instead.
  componentDidUpdate(prevProps: any, prevState: any) {
    if (prevProps !== this.props) {
      const { username } = this.props.authReducer;
      this.loadCart(username);
    }
  }

  updateCart = () => {
    const { username } = this.props.authReducer;
    this.loadCart(username);
  };

  render() {
    const { productsCart } = this.state;
    return (
      <Container>
        <div
          style={{
            backgroundColor: "#3BAFDA", //#3BAFDA 4FC1E9
            display: "flex",
            flexDirection: "row",
            justifyContent: "center",
            marginTop: "10px",
          }}
        >
          <div
            style={{
              flexGrow: 8,
              paddingLeft: "20px",
              color: "#fff",
            }}
          >
            <h3>List Product</h3>
          </div>
          <div
            style={{
              flexGrow: 8,
              paddingLeft: "20px",
              color: "#fff",
            }}
          >
            <h3>Total amount: {this.state.total} $</h3>
          </div>
        </div>

        <TableContainer component={Paper}>
          <Table sx={{ minWidth: 800 }} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell>Product name</TableCell>
                <TableCell align="left">price</TableCell>
                <TableCell align="center">Quantity</TableCell>
                <TableCell align="left">Total</TableCell>
                <TableCell align="left">#</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {this.state.productsCart.length === 0 ? (
                <div style={{ textAlign: "center" }}>
                  <p>{this.state.message}</p>
                </div>
              ) : (
                productsCart.map((p, index) => (
                  <ItemCart key={index} product={p} />
                ))
              )}
            </TableBody>
          </Table>
        </TableContainer>
      </Container>
    );
  }
}

const mapStateToProps = (state: RootState) => {
  return {
    cartReducer: state.cartReducer,
    authReducer: state.authReducer,
    productReducer: state.productReducer,
  };
};

const mapDispatchToProps = (dispatch: Dispatch<AnyAction>) =>
  bindActionCreators(
    {
      getListProducts,
      getBackupProducts,
      changeQuantityCart,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
