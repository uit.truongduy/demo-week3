export const ADD_TO_CART = "ADD_TO_CART";
export const CHANGE_QUANTITY_CART = "CHANGE_QUANTITY_CART";

export interface IProductsCart {
  username: string;
  product: {
    product_id: string;
    quantity: number;
  };
}

//#region add to cart
export interface IAddToCart {
  type: typeof ADD_TO_CART;
  payload: IProductsCart;
}

export const addToCart = (payload: IProductsCart): IAddToCart => {
  return {
    type: ADD_TO_CART,
    payload,
  };
};
//#endregion

//#region remove cart
export interface IChangeQuantityCart {
  type: typeof CHANGE_QUANTITY_CART;
  payload: IProductsCart;
}

export const changeQuantityCart = (
  payload: IProductsCart
): IChangeQuantityCart => {
  return {
    type: CHANGE_QUANTITY_CART,
    payload,
  };
};
//#endregion
