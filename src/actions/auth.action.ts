export const LOGIN_AUTH = "LOGIN_AUTH";

export interface ILogin {
  username: string;
  role: number;
}

//#region login

export interface ILoginAuth {
  type: typeof LOGIN_AUTH;
  payload: ILogin;
}

export const loginAuth = (payload: ILogin): ILoginAuth => {
  return {
    type: LOGIN_AUTH,
    payload,
  };
};
//#endregion
