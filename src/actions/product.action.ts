export const GET_LIST_PRODUCT = "GET_LIST_PRODUCT";
export const CREATE_PRODUCT = "CREATE_PRODUCT";
export const EDIT_PRODUCT = "EDIT_PRODUCT";
export const DELETE_PRODUCT = "DELETE_PRODUCT";
export const GET_BACKUP_PRODUCTS = "GET_BACKUP_PRODUCTS";

export interface IProduct {
  id: string;
  title: string;
  image_url: string;
  price: number;
  description: string;
  category: string;
  tags: string[];
  rating?: number;
}

//#region
export interface IGetBackupProducts {
  type: typeof GET_BACKUP_PRODUCTS;
  payload: any;
}

export const getBackupProducts = (payload: IProduct[]): IGetBackupProducts => {
  return {
    type: GET_BACKUP_PRODUCTS,
    payload,
  };
};
//#endregion

//#region  get list product
export interface IGetListProducts {
  type: typeof GET_LIST_PRODUCT;
  payload: any;
}

export const getListProducts = (payload: any): IGetListProducts => {
  return {
    type: GET_LIST_PRODUCT,
    payload,
  };
};
//#endregion

//#region  create product
export interface ICreateProduct {
  type: typeof CREATE_PRODUCT;
  payload: IProduct;
}

export const createProduct = (payload: IProduct): ICreateProduct => {
  return {
    type: CREATE_PRODUCT,
    payload,
  };
};
//#endregion

//#region  edit product
export interface IEditProduct {
  type: typeof EDIT_PRODUCT;
  payload: IProduct;
}

export const editProduct = (payload: IProduct): IEditProduct => {
  return {
    type: EDIT_PRODUCT,
    payload,
  };
};
//#endregion

//#region  delete product
export interface IDeleteProduct {
  type: typeof DELETE_PRODUCT;
  payload: string;
}

export const deleteProduct = (payload: string): IDeleteProduct => {
  return {
    type: DELETE_PRODUCT,
    payload,
  };
};
//#endregion
