export const CREATE_REVIEW = "CREATE_REVIEW";

export interface IReview {
  productId: string;
  username: string;
  rating: number;
  review: string;
}

//#region create review
export interface ICreateReview {
  type: typeof CREATE_REVIEW;
  payload: IReview;
}

export const createReview = (payload: IReview): ICreateReview => {
  return {
    type: CREATE_REVIEW,
    payload,
  };
};
//#endregion
